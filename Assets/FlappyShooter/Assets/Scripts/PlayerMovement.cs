﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    public float speed = 0.5f, mapWidth;

    private Rigidbody2D rb;

    void Start()
    {
        //Initialization
        rb = GetComponent<Rigidbody2D>();

        rb.isKinematic = false;     //Makes the player kinematic
    }

    void Update()
    {
        if (Input.GetMouseButtonDown(0))        //If screen is pressed
            Jump();     //Then player jumps

        if (transform.position.y > mapWidth || transform.position.y < -mapWidth)        //If the player leaves the boundaries of the map
            FindObjectOfType<GameManager>().EndPanelActivation();       //Then the game is over
    }

    public void Jump()
    {
        rb.Sleep();     //Makes the player stop
        rb.AddForce(Vector2.up * speed);        //Adds an upward force to the player
    }
}