﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Obstacle : MonoBehaviour
{
    public GameObject explosionParticle, bulletParticle;
    public float movementSpeed;
    public AnimationCurve hpCurve;
    public int maxScore = 10000;

    [HideInInspector]
    public int hp = 10;

    private Rigidbody2D rb;
    private TextMeshPro hpText;
    private SpriteRenderer rend;
    private int shootPower;

    void Start()
    {
        //Initializations
        rb = GetComponent<Rigidbody2D>();
        rend = GetComponent<SpriteRenderer>();

        //HP selection
        hp = Mathf.RoundToInt(hpCurve.Evaluate((float)FindObjectOfType<ScoreManager>().score / maxScore));
        hp += Mathf.RoundToInt(Random.Range(-hp / 1.3f, hp / 1.3f));

        rb.AddForce(Vector3.right * -movementSpeed);       //Makes the obstacle move towards the player
        hpText = transform.GetChild(0).GetComponent<TextMeshPro>();
        hpText.text = hp.ToString();        //Writes out HP

        FindObjectOfType<ColorManager>().Coloring(rend, hp);        //Selects a color for the obstacle

        shootPower = PlayerPrefs.GetInt("ShootPower", 1);       //Checks ShootPower
    }

    public void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Bullet"))     //If obstacle collides with a bullet
        {
            Destroy(collision.gameObject);      //Destroys Bullet
            hp -= shootPower;       //Decreases HP of the obstacle
            FindObjectOfType<ScoreManager>().IncrementScore(shootPower);        //Increments score

            FindObjectOfType<ColorManager>().Coloring(rend, hp);

            if (hp <= 0)        //If the obstacle needs to be destroyed
            {
                GameObject tempParticle = Instantiate(explosionParticle, transform.position, Quaternion.identity);      //Spawns an "ExplosionParticle"
                FindObjectOfType<ColorManager>().RandomColor(tempParticle.GetComponent<ParticleSystem>());      //Selects a color for the particle
                Destroy(tempParticle, 4f);      //Destroys particle
                Destroy(gameObject);        //Destroys obstacle
            }

            hpText.text = hp.ToString();        //Writes out HP
        }
        else if (collision.CompareTag("Player"))        //If obstacle collides with the player
            FindObjectOfType<GameManager>().EndPanelActivation();       //Then game is over
    }

    public void StopGameobject()
    {
        rb.Sleep();
    }

    public void StartGameobject()
    {
        rb.AddForce(Vector3.right * -movementSpeed);
    }
}
