﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shoot : MonoBehaviour
{
    public GameObject bullet;

    [HideInInspector]
    public int shootPower;
    [HideInInspector]
    public float shootSpeed;
    [HideInInspector]
    public bool canShoot = true;

    private bool threeBullets = false;

    void Start()
    {
        CheckSpeedAndPower();
    }

    public void CheckSpeedAndPower()
    {
        shootSpeed = PlayerPrefs.GetFloat("ShootSpeed", 1f);
        shootPower = PlayerPrefs.GetInt("ShootPower", 1);
#if UNITY_WEBGL
        shootSpeed = 0.2f;
        shootPower = 1;
        PlayerPrefs.SetFloat("ShootSpeed", shootSpeed);
        PlayerPrefs.SetInt("ShootPower", shootPower);
#endif
    }

    void Update()
    {
        if (canShoot)
        {
            canShoot = false;
            Instantiate(bullet, transform.position, Quaternion.identity);       //Spawns a bullet to the position of the Player

            if (threeBullets)       //If "ThreeBullets" powerup is active
            {
                GameObject bullet1 = Instantiate(bullet, transform.position, Quaternion.identity);       //Spawns a bullet to the position of the Player
                GameObject bullet2 = Instantiate(bullet, transform.position, Quaternion.identity);       //Spawns a bullet to the position of the Player

                bullet1.transform.Rotate(Vector3.forward, -12f);        //Rotates the bullet
                bullet2.transform.Rotate(Vector3.forward, 12f);     //Rotates the bullet
            }

            Invoke("CanShootAgain", shootSpeed);        //Player can shoot again normally
        }
    }

    public void CanShootAgain()
    {
        canShoot = true;
    }

    public void SlowerShoot()
    {
        shootSpeed = PlayerPrefs.GetFloat("ShootSpeed", 1f);
    }

    public void FasterShoot(float duration)
    {
        CancelInvoke("SlowerShoot");
        shootSpeed = 0.01f;
        Invoke("SlowerShoot", duration);
    }

    private void OneBullet()
    {
        threeBullets = false;
    }

    public void ThreeBullets(float duration)
    {
        CancelInvoke("OneBullet");
        threeBullets = true;
        Invoke("OneBullet", duration);
    }
}
