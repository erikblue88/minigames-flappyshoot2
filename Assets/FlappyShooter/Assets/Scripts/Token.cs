﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Token : MonoBehaviour
{
    public float speed = 500f;

    void Start()
    {
        Destroy(gameObject, 10f);       //Destroys Token after X secs
        transform.position = new Vector3(transform.position.x, Random.Range(-1f * FindObjectOfType<PlayerMovement>().mapWidth, (float)FindObjectOfType<PlayerMovement>().mapWidth), transform.position.z);      //Selects a random position on the Y axis
        GetComponent<Rigidbody2D>().AddForce(Vector3.right * -speed);      //Makes the Token move towards the Player
    }

    public void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))     //If Token collides with the Player
        {
            FindObjectOfType<ScoreManager>().IncrementToken();      //Increments tokenCounter
            FindObjectOfType<AudioManager>().TokenSound();      //Plays tokenSound
            GetComponent<ParticleSystem>().Play();      //Plays particle system
            GetComponent<Renderer>().enabled = false;
            GetComponent<Collider2D>().enabled = false;
            Destroy(gameObject, 2f);        //Destroys Token after X seconds
        }
        else if (collision.CompareTag("Obstacle"))      //If Token collides with an Obstacle
            Destroy(gameObject);        //Destroys Token
    }
}
