﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SkinSelect : MonoBehaviour
{
    public Sprite skin1, skin2, skin3;

    void Start()
    {
        SkinCheck();
    }

    public void SkinCheck()
    {
        //Selects a skin for the gameobject

        switch (PlayerPrefs.GetInt("Skin", 0))
        {
            case 0:
                GetComponent<SpriteRenderer>().sprite = skin1;
                break;
            case 1:
                GetComponent<SpriteRenderer>().sprite = skin2;
                break;
            case 2:
                GetComponent<SpriteRenderer>().sprite = skin3;
                break;
        }
    }
}
