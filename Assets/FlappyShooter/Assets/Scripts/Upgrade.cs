﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Upgrade : MonoBehaviour
{
    public GameObject speedUpgradeButton, powerUpgradeButton;
    public Animation notEnoughTokensTextAnim;
    public TextMeshProUGUI fireSpeedText, firePowerText, requiedTokenSpeedText, requiedTokenPowerText;
    public AnimationCurve fireSpeedCost, speedCurve, firePowerCost, powerCurve;
    public int highestUpgradeLevel = 5;

    private int speedUpgradeLevel, powerUpgradeLevel;

    void Start()
    {
        //Initializations
        speedUpgradeLevel = PlayerPrefs.GetInt("SpeedLevel", 0);
        powerUpgradeLevel = PlayerPrefs.GetInt("PowerLevel", 0);
        PlayerPrefs.SetFloat("ShootSpeed", speedCurve.Evaluate((float)speedUpgradeLevel / highestUpgradeLevel));
        PlayerPrefs.SetInt("ShootPower", Mathf.RoundToInt(powerCurve.Evaluate((float)powerUpgradeLevel / highestUpgradeLevel)));        

        UpdateTexts();
    }

    public void UpgradeSpeed()
    {
        if (PlayerPrefs.GetInt("Token", 0) >= Mathf.RoundToInt(fireSpeedCost.Evaluate((float)speedUpgradeLevel / highestUpgradeLevel)))     //If player has enough tokens to upgrade
        {
            FindObjectOfType<ScoreManager>().DecrementToken(Mathf.RoundToInt(fireSpeedCost.Evaluate((float)speedUpgradeLevel / highestUpgradeLevel)));

            PlayerPrefs.SetInt("SpeedLevel", speedUpgradeLevel + 1);
            speedUpgradeLevel = PlayerPrefs.GetInt("SpeedLevel", 1);
            PlayerPrefs.SetFloat("ShootSpeed", speedCurve.Evaluate((float)speedUpgradeLevel / highestUpgradeLevel));

            FindObjectOfType<Shoot>().CheckSpeedAndPower();

            UpdateTexts();
        }
        else      //If player needs more tokens
        {
            notEnoughTokensTextAnim.Play();     //Plays animation
            FindObjectOfType<AudioManager>().NotEnoughTokenSound();     //Plays notEnoughTokenSound
        }
    }

    public void UpgradePower()
    {
        if (PlayerPrefs.GetInt("Token", 0) >= Mathf.RoundToInt(firePowerCost.Evaluate((float)powerUpgradeLevel / highestUpgradeLevel)))     //If player has enough tokens to upgrade
        {
            FindObjectOfType<ScoreManager>().DecrementToken(Mathf.RoundToInt(firePowerCost.Evaluate((float)powerUpgradeLevel / highestUpgradeLevel)));

            PlayerPrefs.SetInt("PowerLevel", powerUpgradeLevel + 1);
            powerUpgradeLevel = PlayerPrefs.GetInt("PowerLevel", 1);
            PlayerPrefs.SetInt("ShootPower", Mathf.RoundToInt(powerCurve.Evaluate((float)powerUpgradeLevel / highestUpgradeLevel)));

            FindObjectOfType<Shoot>().CheckSpeedAndPower();

            UpdateTexts();
        }
        else      //If player needs more tokens
        {
            notEnoughTokensTextAnim.Play();     //Plays animation
            FindObjectOfType<AudioManager>().NotEnoughTokenSound();     //Plays notEnoughTokenSound
        }
    }

    public void UpdateTexts()
    {
        fireSpeedText.text = PlayerPrefs.GetFloat("ShootSpeed", 1f).ToString();
        firePowerText.text = PlayerPrefs.GetInt("ShootPower", 1).ToString();
        requiedTokenSpeedText.text = (Mathf.RoundToInt(fireSpeedCost.Evaluate((float)speedUpgradeLevel / highestUpgradeLevel))).ToString();
        requiedTokenPowerText.text = (Mathf.RoundToInt(firePowerCost.Evaluate((float)powerUpgradeLevel / highestUpgradeLevel))).ToString();

        if (speedUpgradeLevel == highestUpgradeLevel)
            speedUpgradeButton.SetActive(false);

        if (powerUpgradeLevel == highestUpgradeLevel)
            powerUpgradeButton.SetActive(false);
    }
}
