﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectDestroyer : MonoBehaviour {

    public bool destroyBullet, destroyObstacle;

    public void OnTriggerEnter2D(Collider2D collision)
    {
        if (destroyObstacle && collision.CompareTag("Obstacle"))       //If gameObject collides with an obstacle and it can be destroyed
            Destroy(collision.gameObject);      //Destroys the obstacle

        if (destroyBullet && collision.CompareTag("Bullet"))       //If gameObject collides with a bullet and it can be destroyed
            Destroy(collision.gameObject);      //Destroys the bullet        
    }
}
